<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Carousel',
    'LLL:EXT:owlcarousel/Resources/Private/Language/locallang_db.xlf:plugin_desc1'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Content',
    'LLL:EXT:owlcarousel/Resources/Private/Language/locallang_db.xlf:plugin_desc2'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_carousel';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_carousel.xml');

$pluginSignature = str_replace('_','',$_EXTKEY) . '_content';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_content.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Owl Carousel');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_owlcarousel_domain_model_items', 'EXT:owlcarousel/Resources/Private/Language/locallang_csh_tx_owlcarousel_domain_model_items.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_owlcarousel_domain_model_items');

if (TYPO3_MODE == 'BE') {
    $extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY);

    // WizIcon
    $TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses']['RZ\Owlcarousel\Utility\Hook\WizIcon'] =
        $extPath . 'Classes/Utility/Hook/WizIcon.php';
}

// Show tables in page module
$tables = array(
    'tx_owlcarousel_domain_model_items' => 'title',
);

// Traverse tables
foreach($tables as $table => $fields) {
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables'][$table][] = array(
        'fList' => $fields,
        'icon' => true,
    );
}